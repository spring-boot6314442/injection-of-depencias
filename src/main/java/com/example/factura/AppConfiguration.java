package com.example.factura;

import com.example.factura.models.Item;
import com.example.factura.models.Product;
import org.springframework.context.annotation.*;

import java.util.Arrays;
import java.util.List;

@Configuration
@PropertySources(
        @PropertySource(value = "classpath:data.properties", encoding = "UTF-8")
)
public class AppConfiguration {
    @Bean
    List<Item> itemInvoice() {
        Product p1 = new Product("camara", 20);
        Product p2 = new Product("lapiz", 10);
        return Arrays.asList(new Item(p1, 10), new Item(p2, 7));
    }

    @Bean
    @Primary
    List<Item> itemInvoiceSetup() {
        Product p1 = new Product("cpu", 18);
        Product p2 = new Product("monitor", 30);
        return Arrays.asList(new Item(p1, 100), new Item(p2, 23));
    }
}
