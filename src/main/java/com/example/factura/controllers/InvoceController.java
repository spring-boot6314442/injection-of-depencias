package com.example.factura.controllers;

import com.example.factura.models.Invoice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class InvoceController {
    @Autowired
    public Invoice invoice;

    @GetMapping(path = "/invoice")
    public Invoice details(){
        return  invoice;
    }
}
